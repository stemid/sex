# Sex

I found sex.c a long time ago and have no idea where it came from originally.

Decided to re-make it in Python just for kicks.

Should be self-explanatory, it outputs sexual sentences.

## Run

    $ python3 sex.py

## Compile & run

    $ cc -o sex sex.c
    $ ./sex

## Run in repl.it

* [https://repl.it/@StefanMidjich/sexpy](https://repl.it/@StefanMidjich/sexpy)
